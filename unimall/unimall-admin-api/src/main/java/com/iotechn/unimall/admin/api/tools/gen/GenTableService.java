package com.iotechn.unimall.admin.api.tools.gen;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.iotechn.unimall.core.annotation.HttpMethod;
import com.iotechn.unimall.core.annotation.HttpOpenApi;
import com.iotechn.unimall.core.annotation.HttpParam;
import com.iotechn.unimall.core.annotation.HttpParamType;
import com.iotechn.unimall.core.annotation.param.NotNull;
import com.iotechn.unimall.core.exception.ServiceException;
import com.iotechn.unimall.data.domain.tools.gen.GenTable;
import com.iotechn.unimall.data.domain.tools.gen.GenTableColumn;
import com.iotechn.unimall.data.model.Page;

/**
 * 
 * @author dyl
 *
 */
@HttpOpenApi(group = "admin.tools.gen", description = "代码生成")
public interface GenTableService {
	@HttpMethod(description = "代码生成列表", permission = "admin:tools:gen:list", permissionParentName = "系统工具", permissionName = "代码生成")
	public Page<GenTable> list(
			@HttpParam(name = "tableName", type = HttpParamType.COMMON, description = "表名称") String tableName,
			@HttpParam(name = "tableComment", type = HttpParamType.COMMON, description = "表描述") String tableComment,
			@HttpParam(name = "beginTime", type = HttpParamType.COMMON, description = "开始时间") String beginTime,
			@HttpParam(name = "endTime", type = HttpParamType.COMMON, description = "结束时间") String endTime,
			@HttpParam(name = "page", type = HttpParamType.COMMON, description = "页码", valueDef = "1") Integer page,
			@HttpParam(name = "limit", type = HttpParamType.COMMON, description = "页码长度", valueDef = "20") Integer limit)
			throws ServiceException;

	@HttpMethod(description = "数据表列表", permission = "admin:tools:gen:dbList", permissionParentName = "系统工具", permissionName = "代码生成")
	public Page<GenTable> getDbList(
			@HttpParam(name = "tableName", type = HttpParamType.COMMON, description = "表名称") String tableName,
			@HttpParam(name = "tableComment", type = HttpParamType.COMMON, description = "表描述") String tableComment,
			@HttpParam(name = "page", type = HttpParamType.COMMON, description = "页码", valueDef = "1") Integer page,
			@HttpParam(name = "limit", type = HttpParamType.COMMON, description = "页码长度", valueDef = "20") Integer limit)
			throws ServiceException;

	@HttpMethod(description = "查询数据表详情", permission = "admin:tools:gen:get", permissionParentName = "系统工具", permissionName = "代码生成")
	public JSONObject get(@NotNull @HttpParam(name = "id", type = HttpParamType.COMMON, description = "表ID") String id)
			throws ServiceException;

	@HttpMethod(description = "查询数据表字段详情", permission = "admin:tools:genTable:getColumn", permissionParentName = "系统工具", permissionName = "代码生成")
	public List<GenTableColumn> getColumn(
			@HttpParam(name = "talbleId", type = HttpParamType.COMMON, description = "表ID") String talbleId)
			throws ServiceException;

	@HttpMethod(description = "导入表结构", permission = "admin:tools:gen:import", permissionParentName = "系统工具", permissionName = "代码生成")
	public Boolean importTableSave(
			@HttpParam(name = "tables", type = HttpParamType.COMMON, description = "表名") String tables)
			throws ServiceException;

	@HttpMethod(description = "修改表生成信息", permission = "admin:tools:gen:update", permissionParentName = "系统工具", permissionName = "代码生成")
	public Boolean update(
			@NotNull @HttpParam(name = "genTable", type = HttpParamType.COMMON, description = "表信息") GenTable genTable)
			throws ServiceException;

	@HttpMethod(description = "修改表字段生成信息", permission = "admin:tools:gen:updateColumn", permissionParentName = "系统工具", permissionName = "代码生成")
	public Boolean updateColumn(
			@NotNull @HttpParam(name = "genTableColumn", type = HttpParamType.COMMON, description = "表字段信息") GenTableColumn genTableColumn)
			throws ServiceException;

	@HttpMethod(description = "删除", permission = "admin:tools:gen:delete", permissionParentName = "系统工具", permissionName = "代码生成")
	public Boolean delete(
			@NotNull @HttpParam(name = "tableIds", type = HttpParamType.COMMON, description = "表Id") String tableIds)
			throws ServiceException;

	@HttpMethod(description = "代码预览", permission = "admin:tools:gen:preview", permissionParentName = "系统工具", permissionName = "代码生成")
	public JSONObject preview(
			@NotNull @HttpParam(name = "tableId", type = HttpParamType.COMMON, description = "表Id") String tableId)
			throws ServiceException;
}
