package com.iotechn.unimall.admin.api.monitor.server;

import org.springframework.stereotype.Service;

import com.iotechn.unimall.data.domain.monitor.Server;

/**
 * 
 * @author dyl
 *
 */
@Service
public class ServerServiceImpl implements ServerService {

	@Override
	public Server getInfo() throws Exception {
		Server server = new Server();
		server.copyTo();
		return server;
	}

}
