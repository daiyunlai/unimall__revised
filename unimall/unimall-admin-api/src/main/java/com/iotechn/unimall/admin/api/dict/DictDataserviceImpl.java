package com.iotechn.unimall.admin.api.dict;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.iotechn.unimall.core.exception.ServiceException;
import com.iotechn.unimall.data.domain.sys.dict.SysDictData;
import com.iotechn.unimall.data.mapper.sys.dict.SysDictDataMapper;
import com.iotechn.unimall.data.model.Page;
/**
 * 
 * @author dyl
 *
 */
@Service
public class DictDataserviceImpl implements DictDataService{

	@Autowired
	private SysDictDataMapper dictDataMapper;
	
	@Override
	public Boolean add(SysDictData dictData) throws ServiceException {
		return dictDataMapper.insert(dictData)>0;
	}

	@Override
	public Page<SysDictData> list(String dictType, String dictLabel, String status,Integer page, Integer limit)
			throws ServiceException {
		Wrapper<SysDictData> wrapper = new EntityWrapper<SysDictData>();
		if (!StringUtils.isEmpty(dictType)) {
			wrapper.eq("dict_type", dictType);
		}
		if (!StringUtils.isEmpty(dictType)) {
			wrapper.like("dict_label", dictLabel);
		}
		if (!StringUtils.isEmpty(status)) {
			wrapper.like("status", status);
		}
		List<SysDictData> SysDictDataS = dictDataMapper.selectPage(new RowBounds((page - 1) * limit, limit), wrapper);
		Integer count = dictDataMapper.selectCount(wrapper);
		return new Page<SysDictData>(SysDictDataS, page, limit, count);
	}

	@Override
	public Boolean delete(String dictCodes) throws ServiceException {
		String[] ids=dictCodes.split(",");
		Wrapper<SysDictData> wrapper = new EntityWrapper<SysDictData>();
		wrapper.in("dict_code", ids);
		return dictDataMapper.delete(wrapper)>0;
	}

	@Override
	public Boolean update(SysDictData dictData) throws ServiceException {
		return dictDataMapper.updateById(dictData)>0;
	}

	@Override
	public SysDictData get(String dictCode) throws ServiceException {
		return dictDataMapper.selectById(dictCode);
	}

	@Override
	public List<SysDictData> selectDictDataByType(String dictType) throws ServiceException {
		Wrapper<SysDictData> wrapper = new EntityWrapper<SysDictData>();
        if (!StringUtils.isEmpty(dictType)) {
            wrapper.eq("dict_type", dictType);
        }
		return dictDataMapper.selectList(wrapper);
	}

}
