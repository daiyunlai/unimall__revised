package com.iotechn.unimall.admin.api.monitor.online;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.iotechn.unimall.core.annotation.HttpMethod;
import com.iotechn.unimall.core.annotation.HttpOpenApi;
import com.iotechn.unimall.core.annotation.HttpParam;
import com.iotechn.unimall.core.annotation.HttpParamType;
import com.iotechn.unimall.core.exception.ServiceException;

@HttpOpenApi(group = "admin.monitor.online", description = "在线用户管理")
public interface UserOnlineService {
	@HttpMethod(description = "在线用户列表", permission = "admin:monitor:online:list", permissionParentName = "系统管理", permissionName = "在线用户管理")
	public List<JSONObject> list() throws ServiceException;
	
	@HttpMethod(description = "强退用户", permission = "admin:monitor:online:forceLogout", permissionParentName = "系统管理", permissionName = "在线用户管理")
	public Boolean forceLogout(
			@HttpParam(name = "tokenId", type = HttpParamType.COMMON, description = "用户token") String tokenId)
			throws ServiceException;
}
