package com.iotechn.unimall.admin.api.log.loginLog;

import com.iotechn.unimall.core.annotation.HttpMethod;
import com.iotechn.unimall.core.annotation.HttpOpenApi;
import com.iotechn.unimall.core.annotation.HttpParam;
import com.iotechn.unimall.core.annotation.HttpParamType;
import com.iotechn.unimall.core.annotation.param.NotNull;
import com.iotechn.unimall.core.exception.ServiceException;
import com.iotechn.unimall.data.domain.log.SysLogininfor;
import com.iotechn.unimall.data.model.Page;

@HttpOpenApi(group = "admin.log.logininfor", description = "登陆日志管理")
public interface SysLogininforService {
	@HttpMethod(description = "登陆日志列表", permission = "admin:log:logininfor:list", permissionParentName = "日志管理", permissionName = "登陆日志管理")
	public Page<SysLogininfor> list(
			@HttpParam(name = "ipaddr", type = HttpParamType.COMMON, description = "登录地址") String ipaddr,
			@HttpParam(name = "userName", type = HttpParamType.COMMON, description = "用户名称") String userName,
			@HttpParam(name = "status", type = HttpParamType.COMMON, description = "状态") String status,
			@HttpParam(name = "beginTime", type = HttpParamType.COMMON, description = "登陆开始时间") String beginTime,
			@HttpParam(name = "endTime", type = HttpParamType.COMMON, description = "登陆结束时间") String endTime,
			@HttpParam(name = "page", type = HttpParamType.COMMON, description = "页码", valueDef = "1") Integer page,
			@HttpParam(name = "limit", type = HttpParamType.COMMON, description = "页码长度", valueDef = "20") Integer limit)
			throws ServiceException;
	@HttpMethod(description = "删除登陆日志", permission = "admin:log:logininfor:delete", permissionParentName = "日志管理", permissionName = "登陆日志管理")
	public Boolean delete(
			@NotNull @HttpParam(name = "infoIds", type = HttpParamType.COMMON, description = "日志Id") String infoIds)
			throws ServiceException;
	
	@HttpMethod(description = "清空登陆日志", permission = "admin:log:logininfor:clean", permissionParentName = "日志管理", permissionName = "登陆日志管理")
	public Boolean clean()throws ServiceException;
	
	@HttpMethod(description = "导出日志列表", permission = "admin:log:logininfor:export", permissionParentName = "日志管理", permissionName = "登陆日志管理")
	public String export(
			@HttpParam(name = "ipaddr", type = HttpParamType.COMMON, description = "登录地址") String ipaddr,
			@HttpParam(name = "userName", type = HttpParamType.COMMON, description = "用户名称") String userName,
			@HttpParam(name = "status", type = HttpParamType.COMMON, description = "状态") String status,
			@HttpParam(name = "beginTime", type = HttpParamType.COMMON, description = "登陆开始时间") String beginTime,
			@HttpParam(name = "endTime", type = HttpParamType.COMMON, description = "登陆结束时间") String endTime)
			throws ServiceException;
}
