package com.iotechn.unimall.admin.api.log.operLog;

import com.iotechn.unimall.core.annotation.HttpMethod;
import com.iotechn.unimall.core.annotation.HttpOpenApi;
import com.iotechn.unimall.core.annotation.HttpParam;
import com.iotechn.unimall.core.annotation.HttpParamType;
import com.iotechn.unimall.core.annotation.param.NotNull;
import com.iotechn.unimall.core.exception.ServiceException;
import com.iotechn.unimall.data.domain.log.SysOperLog;
import com.iotechn.unimall.data.model.Page;

@HttpOpenApi(group = "admin.log.operlog", description = "操作日志管理")
public interface OperLogService {
	@HttpMethod(description = "操作日志列表", permission = "admin:log:operlog:list", permissionParentName = "日志管理", permissionName = "操作日志管理")
	public Page<SysOperLog> list(
			@HttpParam(name = "title", type = HttpParamType.COMMON, description = "系统模块") String title,
			@HttpParam(name = "operName", type = HttpParamType.COMMON, description = "操作人员") String operName,
			@HttpParam(name = "businessType", type = HttpParamType.COMMON, description = "操作类型") String businessType,
			@HttpParam(name = "beginTime", type = HttpParamType.COMMON, description = "操作开始时间") String beginTime,
			@HttpParam(name = "endTime", type = HttpParamType.COMMON, description = "操作结束时间") String endTime,
			@HttpParam(name = "page", type = HttpParamType.COMMON, description = "页码", valueDef = "1") Integer page,
			@HttpParam(name = "limit", type = HttpParamType.COMMON, description = "页码长度", valueDef = "20") Integer limit)
			throws ServiceException;
	@HttpMethod(description = "删除操作日志", permission = "admin:log:operlog:delete", permissionParentName = "日志管理", permissionName = "操作日志管理")
	public Boolean delete(
			@NotNull @HttpParam(name = "operIds", type = HttpParamType.COMMON, description = "日志Id") String operIds)
			throws ServiceException;
	
	@HttpMethod(description = "清空操作日志", permission = "admin:log:operlog:clean", permissionParentName = "日志管理", permissionName = "操作日志管理")
	public Boolean clean()throws ServiceException;
	
	@HttpMethod(description = "导出日志列表", permission = "admin:log:operlog:export", permissionParentName = "日志管理", permissionName = "操作日志管理")
	public String export(
			@HttpParam(name = "title", type = HttpParamType.COMMON, description = "系统模块") String title,
			@HttpParam(name = "operName", type = HttpParamType.COMMON, description = "操作人员") String operName,
			@HttpParam(name = "businessType", type = HttpParamType.COMMON, description = "操作类型") String businessType,
			@HttpParam(name = "beginTime", type = HttpParamType.COMMON, description = "操作开始时间") String beginTime,
			@HttpParam(name = "endTime", type = HttpParamType.COMMON, description = "操作结束时间") String endTime)
			throws ServiceException;
}
