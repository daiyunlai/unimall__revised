package com.iotechn.unimall.admin.api.monitor.online;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.iotechn.unimall.core.Const;
import com.iotechn.unimall.core.exception.ServiceException;
/**
 * 
 * @author dyl
 *
 */
@Service
public class UserOnlineServiceImpl implements UserOnlineService {
	@Autowired
	private StringRedisTemplate userRedisTemplate;
	
	@Override
	public List<JSONObject> list() throws ServiceException {
		List<JSONObject> loginList=new ArrayList<JSONObject>();
		Set<String> keys = userRedisTemplate.keys(Const.ADMIN_REDIS_PREFIX+"*");
		Iterator<String> it1 = keys.iterator();
		while (it1.hasNext()) {
			String tokenId=it1.next();
			JSONObject json = JSONObject.parseObject(userRedisTemplate.opsForValue().get(tokenId));
			json.put("tokenId", tokenId.split("_")[2]);
			json.put("expireTime", userRedisTemplate.getExpire(tokenId));
			loginList.add(json);
		}
		return loginList;
	}

	@Override
	public Boolean forceLogout(String tokenId) throws ServiceException {
		return userRedisTemplate.delete(Const.ADMIN_REDIS_PREFIX  + tokenId);
	}

}
