package com.iotechn.unimall.admin.api.dict;

import java.util.List;

import com.iotechn.unimall.core.annotation.HttpMethod;
import com.iotechn.unimall.core.annotation.HttpOpenApi;
import com.iotechn.unimall.core.annotation.HttpParam;
import com.iotechn.unimall.core.annotation.HttpParamType;
import com.iotechn.unimall.core.annotation.param.NotNull;
import com.iotechn.unimall.core.exception.ServiceException;
import com.iotechn.unimall.data.domain.sys.dict.SysDictType;
import com.iotechn.unimall.data.model.Page;

@HttpOpenApi(group = "admin.dictType", description = "字典类型服务")
public interface DictTypeService {
	@HttpMethod(description = "创建", permission = "admin:dictType:add", permissionParentName = "系统管理", permissionName = "字典管理")
	public Boolean add(
			@NotNull @HttpParam(name = "dictType", type = HttpParamType.COMMON, description = "字典信息") SysDictType dictType)
			throws ServiceException;

	@HttpMethod(description = "列表", permission = "admin:dictType:list", permissionParentName = "系统管理", permissionName = "字典管理")
	public Page<SysDictType> list(
			@HttpParam(name = "dictName", type = HttpParamType.COMMON, description = "字典名称") String dictName,
			@HttpParam(name = "dictType", type = HttpParamType.COMMON, description = "字典类型") String dictType,
			@HttpParam(name = "page", type = HttpParamType.COMMON, description = "页码", valueDef = "1") Integer page,
			@HttpParam(name = "limit", type = HttpParamType.COMMON, description = "页码长度", valueDef = "20") Integer limit)
			throws ServiceException;

	@HttpMethod(description = "删除", permission = "admin:dictType:delete", permissionParentName = "系统管理", permissionName = "字典管理")
	public Boolean delete(
			@NotNull @HttpParam(name = "dictIds", type = HttpParamType.COMMON, description = "字典Id") String dictIds)
			throws ServiceException;

	@HttpMethod(description = "修改", permission = "admin:dictType:update", permissionParentName = "系统管理", permissionName = "字典管理")
	public Boolean update(
			@NotNull @HttpParam(name = "dictType", type = HttpParamType.COMMON, description = "字典信息") SysDictType dictType)
			throws ServiceException;

	@HttpMethod(description = "查询", permission = "admin:dictType:get", permissionParentName = "系统管理", permissionName = "字典管理")
	public SysDictType get(
			@HttpParam(name = "dictId", type = HttpParamType.COMMON, description = "字典ID") Long dictId)
			throws ServiceException;

	@HttpMethod(description = "字典选择框列表", permission = "admin:dictType:optionselect", permissionParentName = "系统管理", permissionName = "字典管理")
	public List<SysDictType> optionselect()throws ServiceException;


}
