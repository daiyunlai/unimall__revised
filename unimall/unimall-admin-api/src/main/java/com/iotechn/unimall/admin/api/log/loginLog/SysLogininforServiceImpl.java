package com.iotechn.unimall.admin.api.log.loginLog;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.iotechn.unimall.core.exception.ServiceException;
import com.iotechn.unimall.data.domain.log.SysLogininfor;
import com.iotechn.unimall.data.mapper.log.SysLogininforMapper;
import com.iotechn.unimall.data.model.Page;
import com.iotechn.unimall.data.util.ExcelUtil;

/**
 * 
 * @author dyl
 *
 */
@Service
public class SysLogininforServiceImpl implements SysLogininforService {

	@Autowired
	private SysLogininforMapper logininforMapper;

	@Override
	public Page<SysLogininfor> list(String ipaddr, String userName, String status, String beginTime, String endTime,
			Integer page, Integer limit) throws ServiceException {
		Wrapper<SysLogininfor> wrapper = new EntityWrapper<SysLogininfor>();
		if (!StringUtils.isEmpty(ipaddr)) {
			wrapper.like("ipaddr", ipaddr);
		}
		if (!StringUtils.isEmpty(userName)) {
			wrapper.like("user_name", userName);
		}
		if (!StringUtils.isEmpty(status)) {
			wrapper.eq("status", status);
		}
		if (!StringUtils.isEmpty(beginTime) && !StringUtils.isEmpty(endTime)) {
			wrapper.between("login_time", beginTime, endTime);
		}
		wrapper.orderBy("info_id", false);
		List<SysLogininfor> SysOperLogS = logininforMapper.selectPage(new RowBounds((page - 1) * limit, limit), wrapper);
		Integer count = logininforMapper.selectCount(wrapper);
		return new Page<SysLogininfor>(SysOperLogS, page, limit, count);
	}

	@Override
	public Boolean delete(String infoIds) throws ServiceException {
		String[] ids = infoIds.split(",");
		Wrapper<SysLogininfor> wrapper = new EntityWrapper<SysLogininfor>();
		wrapper.in("info_id", ids);
		return logininforMapper.delete(wrapper) > 0;
	}

	@Override
	public Boolean clean() throws ServiceException {
		Wrapper<SysLogininfor> wrapper = new EntityWrapper<SysLogininfor>();
		wrapper.eq("1", 1);
		return logininforMapper.delete(wrapper) > 0;
	}

	@Override
	public String export(String ipaddr, String userName, String status, String beginTime, String endTime)
			throws ServiceException {
		Wrapper<SysLogininfor> wrapper = new EntityWrapper<SysLogininfor>();
		if (!StringUtils.isEmpty(ipaddr)) {
			wrapper.like("ipaddr", ipaddr);
		}
		if (!StringUtils.isEmpty(userName)) {
			wrapper.like("user_name", userName);
		}
		if (!StringUtils.isEmpty(status)) {
			wrapper.eq("status", status);
		}
		if (!StringUtils.isEmpty(beginTime) && !StringUtils.isEmpty(endTime)) {
			wrapper.between("login_time", beginTime, endTime);
		}
		List<SysLogininfor> list = logininforMapper.selectList(wrapper);
		ExcelUtil<SysLogininfor> util = new ExcelUtil<SysLogininfor>(SysLogininfor.class);
		return util.exportExcel(list, "操作日志");
	}

}
