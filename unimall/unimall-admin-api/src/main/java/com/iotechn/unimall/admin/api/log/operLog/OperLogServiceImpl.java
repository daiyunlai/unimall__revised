package com.iotechn.unimall.admin.api.log.operLog;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.iotechn.unimall.core.exception.ServiceException;
import com.iotechn.unimall.data.domain.log.SysOperLog;
import com.iotechn.unimall.data.mapper.log.SysOperLogMapper;
import com.iotechn.unimall.data.model.Page;
import com.iotechn.unimall.data.util.ExcelUtil;

/**
 * 
 * @author dyl
 *
 */
@Service
public class OperLogServiceImpl implements OperLogService {

	@Autowired
	private SysOperLogMapper operLogMapper;

	@Override
	public Page<SysOperLog> list(String title, String operName, String businessType, String beginTime, String endTime,
			Integer page, Integer limit) throws ServiceException {
		Wrapper<SysOperLog> wrapper = new EntityWrapper<SysOperLog>();
		if (!StringUtils.isEmpty(title)) {
			wrapper.like("title", title);
		}
		if (!StringUtils.isEmpty(operName)) {
			wrapper.like("oper_name", operName);
		}
		if (!StringUtils.isEmpty(businessType)) {
			wrapper.eq("business_type", businessType);
		}
		if (!StringUtils.isEmpty(beginTime) && !StringUtils.isEmpty(endTime)) {
			wrapper.between("oper_time", beginTime, endTime);
		}
		wrapper.orderBy("oper_id", false);
		List<SysOperLog> SysOperLogS = operLogMapper.selectPage(new RowBounds((page - 1) * limit, limit), wrapper);
		Integer count = operLogMapper.selectCount(wrapper);
		return new Page<SysOperLog>(SysOperLogS, page, limit, count);
	}

	@Override
	public Boolean delete(String operIds) throws ServiceException {
		String[] ids = operIds.split(",");
		Wrapper<SysOperLog> wrapper = new EntityWrapper<SysOperLog>();
		wrapper.in("oper_id", ids);
		return operLogMapper.delete(wrapper) > 0;
	}

	@Override
	public Boolean clean() throws ServiceException {
		Wrapper<SysOperLog> wrapper = new EntityWrapper<SysOperLog>();
		wrapper.eq("1", 1);
		return operLogMapper.delete(wrapper) > 0;
	}

	@Override
	public String export(String title, String operName, String businessType, String beginTime, String endTime)
			throws ServiceException {
		Wrapper<SysOperLog> wrapper = new EntityWrapper<SysOperLog>();
		if (!StringUtils.isEmpty(title)) {
			wrapper.like("title", title);
		}
		if (!StringUtils.isEmpty(operName)) {
			wrapper.like("oper_name", operName);
		}
		if (!StringUtils.isEmpty(businessType)) {
			wrapper.eq("business_type", businessType);
		}
		if (!StringUtils.isEmpty(beginTime) && !StringUtils.isEmpty(endTime)) {
			wrapper.between("oper_time", beginTime, endTime);
		}
		List<SysOperLog> list = operLogMapper.selectList(wrapper);
		ExcelUtil<SysOperLog> util = new ExcelUtil<SysOperLog>(SysOperLog.class);
		return util.exportExcel(list, "操作日志");
	}

}
