package com.iotechn.unimall.admin.api.monitor.server;

import com.iotechn.unimall.core.annotation.HttpMethod;
import com.iotechn.unimall.core.annotation.HttpOpenApi;
import com.iotechn.unimall.data.domain.monitor.Server;

@HttpOpenApi(group = "admin.monitor.server", description = "服务监控数据")
public interface ServerService {
	@HttpMethod(description = "获取服务监控数据", permission = "admin:monitor:server:getInfo", permissionParentName = "系统管理", permissionName = "服务监控数据")
	public Server getInfo()throws Exception;
}
