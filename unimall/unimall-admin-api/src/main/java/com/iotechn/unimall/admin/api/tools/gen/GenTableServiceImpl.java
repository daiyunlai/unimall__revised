package com.iotechn.unimall.admin.api.tools.gen;

import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.iotechn.unimall.core.exception.AdminServiceException;
import com.iotechn.unimall.core.exception.ExceptionDefinition;
import com.iotechn.unimall.core.exception.ServiceException;
import com.iotechn.unimall.core.util.StringUtils;
import com.iotechn.unimall.data.domain.tools.gen.GenTable;
import com.iotechn.unimall.data.domain.tools.gen.GenTableColumn;
import com.iotechn.unimall.data.mapper.tools.gen.GenTableColumnMapper;
import com.iotechn.unimall.data.mapper.tools.gen.GenTableMapper;
import com.iotechn.unimall.data.model.Page;
import com.iotechn.unimall.data.util.GenUtils;
import com.iotechn.unimall.data.util.SessionUtil;
import com.iotechn.unimall.data.util.VelocityInitializer;
import com.iotechn.unimall.data.util.VelocityUtils;

/**
 * 
 * @author dyl
 *
 */
@Service
public class GenTableServiceImpl implements GenTableService {

	@Autowired
	private GenTableMapper tableMapper;

	@Autowired
	private GenTableColumnMapper tableColumnMapper;

	@Override
	public Page<GenTable> list(String tableName, String tableComment, String beginTime, String endTime, Integer page,
			Integer limit) throws ServiceException {
		Wrapper<GenTable> wrapper = new EntityWrapper<GenTable>();
		if (!StringUtils.isEmpty(tableName)) {
			wrapper.eq("table_name", tableName);
		}
		if (!StringUtils.isEmpty(tableComment)) {
			wrapper.like("table_comment", tableComment);
		}
		if (!StringUtils.isEmpty(beginTime) && !StringUtils.isEmpty(endTime)) {
			wrapper.between("create_time", beginTime, endTime);
		}
		List<GenTable> GenTableS = tableMapper.selectPage(new RowBounds((page - 1) * limit, limit), wrapper);
		Integer count = tableMapper.selectCount(wrapper);
		return new Page<GenTable>(GenTableS, page, limit, count);
	}

	@Override
	public Page<GenTable> getDbList(String tableName, String tableComment, Integer page, Integer limit)
			throws ServiceException {
		GenTable genTable = new GenTable();
		genTable.setTableName(tableName);
		genTable.setTableComment(tableComment);
		List<GenTable> GenTableS = tableMapper.selectDbTableList(new RowBounds((page - 1) * limit, limit), genTable);
		Integer count = tableMapper.selectDbTableCount(genTable);
		return new Page<GenTable>(GenTableS, page, limit, count);
	}

	@Override
	public JSONObject get(String id) throws ServiceException {
		JSONObject result = new JSONObject();
		Wrapper<GenTableColumn> wrapper = new EntityWrapper<GenTableColumn>();
		wrapper.eq("table_id", id);

		GenTable table = tableMapper.selectById(id);
		List<GenTableColumn> list = tableColumnMapper.selectList(wrapper);

		result.put("info", table);
		result.put("rows", list);
		return result;
	}

	@Override
	public List<GenTableColumn> getColumn(String talbleId) throws ServiceException {
		Wrapper<GenTableColumn> wrapper = new EntityWrapper<GenTableColumn>();
		wrapper.eq("table_id", talbleId);
		List<GenTableColumn> list = tableColumnMapper.selectList(wrapper);
		return list;
	}

	@Override
	public Boolean importTableSave(String tables) throws ServiceException {
		String[] ids = tables.split(",");
		List<GenTable> tableList = tableMapper.selectDbTableListByNames(ids);
		String operName = SessionUtil.getAdmin().getUsername();
		for (GenTable table : tableList) {
			try {
				String tableName = table.getTableName();
				GenUtils.initTable(table, operName);
				int row = tableMapper.insert(table);
				if (row > 0) {
					// 保存列信息
					List<GenTableColumn> genTableColumns = tableColumnMapper.selectDbTableColumnsByName(tableName);
					for (GenTableColumn column : genTableColumns) {
						GenUtils.initColumnField(column, table);
						tableColumnMapper.insert(column);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new AdminServiceException(ExceptionDefinition.TABLE_IMPORT_ERROR);
			}
		}
		return true;
	}

	@Override
	public Boolean update(GenTable genTable) throws ServiceException {
		return tableMapper.updateById(genTable) > 0;
	}

	@Override
	public Boolean updateColumn(GenTableColumn genTableColumn) throws ServiceException {
		return tableColumnMapper.updateById(genTableColumn) > 0;
	}

	@Override
	public Boolean delete(String tableIds) throws ServiceException {
		String[] ids = tableIds.split(",");
		Wrapper<GenTable> wrapper = new EntityWrapper<GenTable>();
		wrapper.in("table_id", ids);
		return tableMapper.delete(wrapper) > 0;
	}

	@Override
	public JSONObject preview(String tableId) throws ServiceException {
		Map<String, String> dataMap = new LinkedHashMap<>();
		// 查询表信息
		GenTable table = tableMapper.selectById(tableId);
		Wrapper<GenTableColumn> wrapper = new EntityWrapper<GenTableColumn>();
		wrapper.eq("table_id", table.getTableId());
		// 查询列信息
		List<GenTableColumn> columns =tableColumnMapper.selectList(wrapper);
		table.setColumns(columns);
		setPkColumn(table, columns);
		VelocityInitializer.initVelocity();

		VelocityContext context = VelocityUtils.prepareContext(table);

		// 获取模板列表
		List<String> templates = VelocityUtils.getTemplateList(table.getTplCategory());
		for (String template : templates) {
			// 渲染模板
			StringWriter sw = new StringWriter();
			Template tpl = Velocity.getTemplate(template, "UTF-8");
			tpl.merge(context, sw);
			dataMap.put(template, sw.toString());
		}
		return JSONObject.parseObject(JSONObject.toJSONString(dataMap));
	}

	public void setPkColumn(GenTable table, List<GenTableColumn> columns) {
		for (GenTableColumn column : columns) {
			if (column.isPk()) {
				table.setPkColumn(column);
				break;
			}
		}
		if (StringUtils.isNull(table.getPkColumn())) {
			table.setPkColumn(columns.get(0));
		}
	}
}
