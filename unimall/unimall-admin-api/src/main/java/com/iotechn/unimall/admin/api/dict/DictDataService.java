package com.iotechn.unimall.admin.api.dict;

import java.util.List;

import com.iotechn.unimall.core.annotation.HttpMethod;
import com.iotechn.unimall.core.annotation.HttpOpenApi;
import com.iotechn.unimall.core.annotation.HttpParam;
import com.iotechn.unimall.core.annotation.HttpParamType;
import com.iotechn.unimall.core.annotation.param.NotNull;
import com.iotechn.unimall.core.exception.ServiceException;
import com.iotechn.unimall.data.domain.sys.dict.SysDictData;
import com.iotechn.unimall.data.model.Page;

@HttpOpenApi(group = "admin.dictData", description = "字典数据服务")
public interface DictDataService {
	@HttpMethod(description = "创建", permission = "admin:dictData:add", permissionParentName = "系统管理", permissionName = "字典管理")
	public Boolean add(
			@NotNull @HttpParam(name = "dictData", type = HttpParamType.COMMON, description = "字典信息") SysDictData dictData)
			throws ServiceException;

	@HttpMethod(description = "列表", permission = "admin:dictData:list", permissionParentName = "系统管理", permissionName = "字典管理")
	public Page<SysDictData> list(
			@HttpParam(name = "dictType", type = HttpParamType.COMMON, description = "字典类型") String dictType,
			@HttpParam(name = "dictLabel", type = HttpParamType.COMMON, description = "字典标签") String dictLabel,
			@HttpParam(name = "status", type = HttpParamType.COMMON, description = "状态") String status,
			@HttpParam(name = "page", type = HttpParamType.COMMON, description = "页码", valueDef = "1") Integer page,
			@HttpParam(name = "limit", type = HttpParamType.COMMON, description = "页码长度", valueDef = "20") Integer limit)
			throws ServiceException;

	@HttpMethod(description = "删除", permission = "admin:dictData:delete", permissionParentName = "系统管理", permissionName = "字典管理")
	public Boolean delete(
			@NotNull @HttpParam(name = "dictCodes", type = HttpParamType.COMMON, description = "字典Id") String dictCodes)
			throws ServiceException;

	@HttpMethod(description = "修改", permission = "admin:dictData:update", permissionParentName = "系统管理", permissionName = "字典管理")
	public Boolean update(
			@NotNull @HttpParam(name = "dictData", type = HttpParamType.COMMON, description = "字典信息") SysDictData dictData)
			throws ServiceException;

	@HttpMethod(description = "查询", permission = "admin:dictData:get", permissionParentName = "系统管理", permissionName = "字典管理")
	public SysDictData get(
			@HttpParam(name = "dictCode", type = HttpParamType.COMMON, description = "字典ID") String dictCode)
			throws ServiceException;

	@HttpMethod(description = "根据字典类型查询所属数据", permission = "admin:dictData:selectDictDataByType", permissionParentName = "系统管理", permissionName = "字典管理")
	public List<SysDictData> selectDictDataByType(
			@HttpParam(name = "dictType", type = HttpParamType.COMMON, description = "字典类型") String dictType)
			throws ServiceException;

}
