package com.iotechn.unimall.launcher.utils;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;

import javax.servlet.http.HttpServletRequest;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
/**
 * 获取请求IP地址、浏览器相关信息
 */
public class BrowserData {
	
	@Value("${api01.aliyun.appcode}")
	private static String appcode;
	
	/**
	 * 获取发起请求的IP地址
	 */

	public static String getIpAddr(HttpServletRequest request) {
		if (request == null) {
			return "unknown";
		}
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("X-Forwarded-For");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("X-Real-IP");
		}

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		return "0:0:0:0:0:0:0:1".equals(ip) ? "127.0.0.1" : ip;
	}

	/**
	 * 获取发起请求的浏览器名称
	 */
	public static String getBrowserName(HttpServletRequest request) {
		String header = request.getHeader("User-Agent");
		UserAgent userAgent = UserAgent.parseUserAgentString(header);
		Browser browser = userAgent.getBrowser();
		return browser.getName();
	}

	/**
	 * 获取发起请求的浏览器版本号
	 */
	public static String getBrowserVersion(HttpServletRequest request) {
		String header = request.getHeader("User-Agent");
		UserAgent userAgent = UserAgent.parseUserAgentString(header);
		// 获取浏览器信息
		Browser browser = userAgent.getBrowser();
		// 获取浏览器版本号
		Version version = browser.getVersion(header);
		return version.getVersion();
	}

	/**
	 * 获取发起请求的操作系统名称
	 */
	public static String getOsName(HttpServletRequest request) {
		String header = request.getHeader("User-Agent");
		UserAgent userAgent = UserAgent.parseUserAgentString(header);
		OperatingSystem operatingSystem = userAgent.getOperatingSystem();
		return operatingSystem.getName();
	}

	/**
	 * 根据IP地址判断客户端属于哪个国家或地区
	 * 
	 * @param content        请求的参数 格式为：name=xxx&pwd=xxx
	 * @param encodingString 服务器端请求编码。如GBK,UTF-8等
	 * @return
	 */
	public static String getAddresses(String IP) {
		if (IP.equals("127.0.0.1")||IP.equals("localhost")) {
			return "内网IP";
		}
		String host = "https://api01.aliyun.venuscn.com";
		String path = "/ip";
		String method = "GET";
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "APPCODE " + appcode);
		Map<String, String> querys = new HashMap<String, String>();
		querys.put("ip",IP);
		try {
			HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
			JSONObject data = ((JSONObject) JSONObject.toJSON(response.getEntity())).getJSONObject("data");
			return data.getString("country") + "." + data.getString("region") + "." + data.getString("city");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "未知";
	}

}
