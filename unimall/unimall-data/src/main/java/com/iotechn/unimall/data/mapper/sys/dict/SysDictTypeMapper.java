package com.iotechn.unimall.data.mapper.sys.dict;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.unimall.data.domain.sys.dict.SysDictType;

public interface SysDictTypeMapper extends BaseMapper<SysDictType>{
	public SysDictType checkDictTypeUnique(SysDictType dictType);
	public List<SysDictType> selectDictTypeAll();
}
