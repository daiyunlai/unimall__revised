package com.iotechn.unimall.data.config.db;

import java.util.Date;

import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.mapper.MetaObjectHandler;
import com.iotechn.unimall.data.util.SessionUtil;

@Component
public class MetaObjectHandlerConfig extends MetaObjectHandler {

	@Override
	public void insertFill(MetaObject metaObject) {
		try {
			setFieldValByName("createTime", new Date(), metaObject);
			setFieldValByName("createBy", SessionUtil.getAdmin().getUsername(), metaObject);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	public void updateFill(MetaObject metaObject) {
		try {
			setFieldValByName("updateTime", new Date(), metaObject);
			setFieldValByName("updateBy", SessionUtil.getAdmin().getUsername(), metaObject);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
