package com.iotechn.unimall.data.mapper.tools.gen;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.unimall.data.domain.tools.gen.GenTableColumn;

public interface GenTableColumnMapper extends BaseMapper<GenTableColumn>{
	
	public List<GenTableColumn> selectDbTableColumnsByName(String tableName);
}
