package com.iotechn.unimall.data.mapper.log;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.unimall.data.domain.log.SysLogininfor;

public interface SysLogininforMapper extends BaseMapper<SysLogininfor>{

}
