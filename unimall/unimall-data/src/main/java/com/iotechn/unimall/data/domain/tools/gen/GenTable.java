package com.iotechn.unimall.data.domain.tools.gen;

import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.iotechn.unimall.core.util.StringUtils;
import com.iotechn.unimall.data.component.GenConstants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 业务表 gen_table
 * 
 * @author jlb
 */

@Data
@TableName("sys_gen_table")
@NoArgsConstructor
@AllArgsConstructor
public class GenTable {
	private static final long serialVersionUID = 1L;

	/** 编号 */
	@TableId("table_id")
	private Long tableId;

	/** 表名称 */
	@NotBlank(message = "表名称不能为空")
	@TableField("table_name")
	private String tableName;

	/** 表描述 */
	@NotBlank(message = "表描述不能为空")
	@TableField("table_comment")
	private String tableComment;

	/** 实体类名称(首字母大写) */
	@NotBlank(message = "实体类名称不能为空")
	@TableField("class_name")
	private String className;

	/** 使用的模板（crud单表操作 tree树表操作） */
	@TableField("tpl_category")
	private String tplCategory;

	/** 生成包路径 */
	@NotBlank(message = "生成包路径不能为空")
	@TableField("package_name")
	private String packageName;

	/** 生成模块名 */
	@NotBlank(message = "生成模块名不能为空")
	@TableField("module_name")
	private String moduleName;

	/** 生成业务名 */
	@NotBlank(message = "生成业务名不能为空")
	@TableField("business_name")
	private String businessName;

	/** 生成功能名 */
	@NotBlank(message = "生成功能名不能为空")
	@TableField("function_name")
	private String functionName;

	/** 生成作者 */
	@NotBlank(message = "作者不能为空")
	@TableField("function_author")
	private String functionAuthor;

	/** 创建者 */
	@TableField(value = "create_by", fill = FieldFill.INSERT)
	private String createBy;

	/** 创建时间 */
	@TableField(value = "create_time", fill = FieldFill.INSERT)
	private Date createTime;

	/** 更新者 */
	@TableField(value = "update_by", fill = FieldFill.UPDATE)
	private String updateBy;

	/** 更新时间 */
	@TableField(value = "update_time", fill = FieldFill.UPDATE)
	private Date updateTime;

	/** 主键信息 */
	@TableField(exist = false)
	@JSONField(serialize = false)
	private GenTableColumn pkColumn;

	/** 表列信息 */
	@Valid
	@TableField(exist = false)
	@JSONField(serialize = false)
	private List<GenTableColumn> columns;

	/** 其它生成选项 */
	@TableField("options")
	private String options;

	/** 树编码字段 */
	@TableField(exist = false)
	private String treeCode;

	/** 树父编码字段 */
	@TableField(exist = false)
	private String treeParentCode;

	/** 树名称字段 */
	@TableField(exist = false)
	private String treeName;

	@JSONField(serialize = false)
	public boolean isTree() {
		return isTree(this.tplCategory);
	}
	
	public static boolean isTree(String tplCategory) {
		return tplCategory != null && StringUtils.equals(GenConstants.TPL_TREE, tplCategory);
	}
	@JSONField(serialize = false)
	public boolean isCrud() {
		return isCrud(this.tplCategory);
	}

	public static boolean isCrud(String tplCategory) {
		return tplCategory != null && StringUtils.equals(GenConstants.TPL_CRUD, tplCategory);
	}
	@JSONField(serialize = false)
	public boolean isSuperColumn(String javaField) {
		return isSuperColumn(this.tplCategory, javaField);
	}

	public static boolean isSuperColumn(String tplCategory, String javaField) {
		if (isTree(tplCategory)) {
			StringUtils.equalsAnyIgnoreCase(javaField, GenConstants.TREE_ENTITY);
		}
		return StringUtils.equalsAnyIgnoreCase(javaField, GenConstants.BASE_ENTITY);
	}
}