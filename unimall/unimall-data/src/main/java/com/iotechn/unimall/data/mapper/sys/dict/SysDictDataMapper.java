package com.iotechn.unimall.data.mapper.sys.dict;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.unimall.data.domain.sys.dict.SysDictData;
import com.iotechn.unimall.data.domain.sys.dict.SysDictType;

public interface SysDictDataMapper extends BaseMapper<SysDictData>{
	
	public String checkDictTypeUnique(SysDictType dictType);
}
