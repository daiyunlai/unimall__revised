package com.iotechn.unimall.data.domain.sys.dict;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.iotechn.unimall.core.framework.aspectj.lang.annotaion.Excel;
import com.iotechn.unimall.core.framework.aspectj.lang.annotaion.Excel.ColumnType;

import lombok.Data;

/**
 * 字典类型表 sys_dict_type
 * 
 * @author jlb
 */
@Data
@TableName("sys_dict_type")
public class SysDictType {
	private static final long serialVersionUID = 1L;

	/** 字典主键 */
	@Excel(name = "字典主键", cellType = ColumnType.NUMERIC)
	@TableId("dict_id")
	private Long dictId;

	/** 字典名称 */
	@Excel(name = "字典名称")
	@TableField("dict_name")
	private String dictName;

	/** 字典类型 */
	@Excel(name = "字典类型")
	@TableField("dict_type")
	private String dictType;

	/** 状态（0正常 1停用） */
	@Excel(name = "状态", readConverterExp = "0=正常,1=停用")
	private String status;

	@Excel(name = "备注")
	private String remark;

	 /** 创建者 */
	@TableField(value="create_by",fill = FieldFill.INSERT)
    private String createBy;

    /** 创建时间 */
	@TableField(value="create_time",fill = FieldFill.INSERT)
    private Date createTime;

    /** 更新者 */
	@TableField(value="update_by",fill = FieldFill.UPDATE)
    private String updateBy;

    /** 更新时间 */
	@TableField(value="update_time",fill = FieldFill.UPDATE)
    private Date updateTime;


	@NotBlank(message = "字典名称不能为空")
	@Size(min = 0, max = 100, message = "字典类型名称长度不能超过100个字符")
	public String getDictName() {
		return dictName;
	}

	@NotBlank(message = "字典类型不能为空")
	@Size(min = 0, max = 100, message = "字典类型类型长度不能超过100个字符")
	public String getDictType() {
		return dictType;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("dictId", getDictId())
				.append("dictName", getDictName()).append("dictType", getDictType()).append("status", getStatus())
				.append("remark", remark).toString();
	}
}
