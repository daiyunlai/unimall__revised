package com.iotechn.unimall.data.domain.log;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.iotechn.unimall.core.framework.aspectj.lang.annotaion.Excel;
import com.iotechn.unimall.core.framework.aspectj.lang.annotaion.Excel.ColumnType;

import lombok.Data;

/**
 * 系统访问记录表 sys_logininfor
 * 
 * @author jlb
 */
@Data
@TableName("sys_logininfor")
public class SysLogininfor {
	private static final long serialVersionUID = 1L;

	/** ID */
	@Excel(name = "序号")
	@TableId("info_id")
	private Long infoId;

	/** 用户账号 */
	@Excel(name = "用户账号")
	@TableField("user_name")
	private String userName;

	/** 登录状态 0成功 1失败 */
	@Excel(name = "登录状态", readConverterExp = "0=成功,1=失败")
	@TableField("status")
	private String status;

	/** 登录IP地址 */
	@Excel(name = "登录地址")
	@TableField("ipaddr")
	private String ipaddr;

	/** 登录地点 */
	@Excel(name = "登录地点")
	@TableField("login_location")
	private String loginLocation;

	/** 浏览器类型 */
	@Excel(name = "浏览器")
	@TableField("browser")
	private String browser;

	/** 操作系统 */
	@Excel(name = "操作系统")
	@TableField("os")
	private String os;

	/** 提示消息 */
	@Excel(name = "提示消息")
	@TableField("msg")
	private String msg;

	/** 访问时间 */
	@Excel(name = "访问时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	@TableField("login_time")
	private Date loginTime;

	public Long getInfoId() {
		return infoId;
	}

	public void setInfoId(Long infoId) {
		this.infoId = infoId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIpaddr() {
		return ipaddr;
	}

	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}

	public String getLoginLocation() {
		return loginLocation;
	}

	public void setLoginLocation(String loginLocation) {
		this.loginLocation = loginLocation;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
}