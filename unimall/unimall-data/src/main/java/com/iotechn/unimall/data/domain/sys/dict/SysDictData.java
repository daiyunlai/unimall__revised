package com.iotechn.unimall.data.domain.sys.dict;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.iotechn.unimall.core.common.UserConstants;
import com.iotechn.unimall.core.framework.aspectj.lang.annotaion.Excel;
import com.iotechn.unimall.core.framework.aspectj.lang.annotaion.Excel.ColumnType;
import com.iotechn.unimall.data.domain.SuperDO;

import lombok.Data;

/**
 * 字典数据表 sys_dict_data
 * 
 * @author jlb
 */
@Data
@TableName("sys_dict_data")
public class SysDictData {
	private static final long serialVersionUID = 1L;

	/** 字典编码 */
	@Excel(name = "字典编码", cellType = ColumnType.NUMERIC)
	@TableId("dict_code")
	private Long dictCode;

	/** 字典排序 */
	@Excel(name = "字典排序", cellType = ColumnType.NUMERIC)
	@TableField("dict_sort")
	private Long dictSort;

	/** 字典标签 */
	@Excel(name = "字典标签")
	@TableField("dict_label")
	private String dictLabel;

	/** 字典键值 */
	@Excel(name = "字典键值")
	@TableField("dict_value")
	private String dictValue;

	/** 字典类型 */
	@Excel(name = "字典类型")
	@TableField("dict_type")
	private String dictType;

	/** 样式属性（其他样式扩展） */
	@TableField("css_class")
	private String cssClass;

	/** 表格字典样式 */
	@TableField("list_class")
	private String listClass;

	/** 是否默认（Y是 N否） */
	@Excel(name = "是否默认", readConverterExp = "Y=是,N=否")
	@TableField("is_default")
	private String isDefault;

	/** 状态（0正常 1停用） */
	@Excel(name = "状态", readConverterExp = "0=正常,1=停用")
	private String status;

	@Excel(name = "备注")
	private String remark;

	/** 创建者 */
	@TableField(value = "create_by", fill = FieldFill.INSERT)
	private String createBy;

	/** 创建时间 */
	@TableField(value = "create_time", fill = FieldFill.INSERT)
	private Date createTime;

	/** 更新者 */
	@TableField(value = "update_by", fill = FieldFill.UPDATE)
	private String updateBy;

	/** 更新时间 */
	@TableField(value = "update_time", fill = FieldFill.UPDATE)
	private Date updateTime;

	@NotBlank(message = "字典标签不能为空")
	@Size(min = 0, max = 100, message = "字典标签长度不能超过100个字符")
	public String getDictLabel() {
		return dictLabel;
	}

	public void setDictLabel(String dictLabel) {
		this.dictLabel = dictLabel;
	}

	@NotBlank(message = "字典键值不能为空")
	@Size(min = 0, max = 100, message = "字典键值长度不能超过100个字符")
	public String getDictValue() {
		return dictValue;
	}

	@NotBlank(message = "字典类型不能为空")
	@Size(min = 0, max = 100, message = "字典类型长度不能超过100个字符")
	public String getDictType() {
		return dictType;
	}

	@Size(min = 0, max = 100, message = "样式属性长度不能超过100个字符")
	public String getCssClass() {
		return cssClass;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("dictCode", getDictCode())
				.append("dictSort", getDictSort()).append("dictLabel", getDictLabel())
				.append("dictValue", getDictValue()).append("dictType", getDictType()).append("cssClass", getCssClass())
				.append("listClass", getListClass()).append("isDefault", getIsDefault()).append("status", getStatus())
				.toString();
	}
}
