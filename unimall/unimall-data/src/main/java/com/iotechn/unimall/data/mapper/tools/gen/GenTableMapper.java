package com.iotechn.unimall.data.mapper.tools.gen;

import java.util.List;

import org.apache.ibatis.session.RowBounds;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.unimall.data.domain.tools.gen.GenTable;
import com.iotechn.unimall.data.domain.tools.gen.GenTableColumn;

public interface GenTableMapper extends BaseMapper<GenTable>{
	
	/**
     * 查询据库列表
     * 
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
	public List<GenTable> selectDbTableList(RowBounds rowBounds,GenTable genTable);
	public Integer selectDbTableCount(GenTable genTable);
    public List<GenTable> selectDbTableListByNames(String[] tableNames);
    
}
