import request from '@/utils/request'

// 查询服务器详细
export function getServer() {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.monitor.server',
      _mt: 'getInfo'
    }
  })
}
