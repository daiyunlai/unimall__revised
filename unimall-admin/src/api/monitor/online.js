import request from '@/utils/request'

// 查询在线用户列表
export function list() {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.monitor.online',
      _mt: 'list'
    }
  })
}

// 强退用户
export function forceLogout(tokenId) {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.monitor.online',
      _mt: 'forceLogout',
      tokenId: tokenId
    }
  })
}
