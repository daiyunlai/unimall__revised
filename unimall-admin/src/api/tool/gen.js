import request from '@/utils/request'

// 查询生成表数据
export function listTable(query) {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.tools.gen',
      _mt: 'list',
      ...query
    }
  })
}
// 查询db数据库列表
export function listDbTable(query) {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.tools.gen',
      _mt: 'getDbList',
      ...query
    }
  })
}

// 查询表详细信息
export function getGenTable(tableId) {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.tools.gen',
      _mt: 'get',
      id: tableId
    }
  })
}

// 修改代码生成信息
export function updateGenTable(data) {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.tools.gen',
      _mt: 'update',
      genTable: data
    }
  })
}
// 修改代码生成字段信息
export function updateGenTableColumns(data) {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.tools.gen',
      _mt: 'updateColumn',
      genTableColumn: data
    }
  })
}
// 导入表
export function importTable(data) {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.tools.gen',
      _mt: 'importTableSave',
      ...data
    }
  })
}
// 预览生成代码
export function previewTable(tableId) {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.tools.gen',
      _mt: 'preview',
      tableId: tableId
    }
  })
}
// 删除表数据
export function delTable(tableIds) {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.tools.gen',
      _mt: 'delete',
      tableIds: tableIds
    }
  })
}

