import request from '@/utils/request'

// 查询字典类型列表
export function listType(query) {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.dictType',
      _mt: 'list',
      ...query
    }
  })
}

// 查询字典类型详细
export function getType(dictId) {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.dictType',
      _mt: 'get',
      dictId: dictId
    }
  })
}

// 新增字典类型
export function addType(data) {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.dictType',
      _mt: 'add',
      dictType: JSON.stringify(data)
    }
  })
}

// 修改字典类型
export function updateType(data) {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.dictType',
      _mt: 'update',
      dictType: JSON.stringify(data)
    }
  })
}

// 删除字典类型
export function delType(dictIds) {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.dictType',
      _mt: 'delete',
      dictIds: dictIds
    }
  })
}

// 获取字典选择框列表
export function optionselect() {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.dictType',
      _mt: 'optionselect'
    }
  })
}
