import request from '@/utils/request'

// 查询字典数据列表
export function listData(query) {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.dictData',
      _mt: 'list',
      ...query
    }
  })
}

// 查询字典数据详细
export function getData(dictCode) {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.dictData',
      _mt: 'get',
      dictCode: dictCode
    }
  })
}

// 根据字典类型查询字典数据信息
export function getDicts(dictType) {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.dictData',
      _mt: 'selectDictDataByType',
      dictType: dictType
    }
  })
}

// 新增字典数据
export function addData(data) {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.dictData',
      _mt: 'add',
      dictData: data
    }
  })
}

// 修改字典数据
export function updateData(data) {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.dictData',
      _mt: 'update',
      dictData: data
    }
  })
}

// 删除字典数据
export function delData(dictCodes) {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.dictData',
      _mt: 'delete',
      dictCodes: dictCodes
    }
  })
}

