import request from '@/utils/request'

// 查询操作日志列表
export function list(query) {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.log.operlog',
      _mt: 'list',
      ...query
    }
  })
}

// 删除操作日志
export function delOperlog(operIds) {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.log.operlog',
      _mt: 'delete',
      operIds: operIds
    }
  })
}

// 清空操作日志
export function cleanOperlog() {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.log.operlog',
      _mt: 'clean'
    }
  })
}

// 导出操作日志
export function exportOperlog(query) {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.log.operlog',
      _mt: 'export',
      ...query
    }
  })
}
