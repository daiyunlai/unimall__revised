import request from '@/utils/request'

// 查询登录日志列表
export function list(query) {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.log.logininfor',
      _mt: 'list',
      ...query
    }
  })
}

// 删除登录日志
export function delLogininfor(infoId) {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.log.logininfor',
      _mt: 'delete',
      infoIds: infoId
    }
  })
}

// 清空登录日志
export function cleanLogininfor() {
  return request({
    method: 'post',
    params: {
      _gp: 'admin.log.logininfor',
      _mt: 'clean'
    }
  })
}

// 导出登录日志
export function exportLogininfor(query) {
  return request({
    method: 'get',
    params: {
      _gp: 'admin.log.logininfor',
      _mt: 'export',
      ...query
    }
  })
}
