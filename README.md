# ubimall 改版

#### 介绍
此版本是在[unimall](http://https://gitee.com/iotechn/unimall)基础上新添加了代码生成、字典、日志、监控等功能，分享出来给大家

具体介绍大家请移步[unimall](http://https://gitee.com/iotechn/unimall)查看，这里只简单说一下我这个版本新增的功能


![字典管理页面](https://images.gitee.com/uploads/images/2019/1225/221328_82165099_2052324.png "字典管理.png")
![在线用户监控](https://images.gitee.com/uploads/images/2019/1225/221427_f2d1ed7e_2052324.png "在线用户.png")
![数据监控](https://images.gitee.com/uploads/images/2019/1225/221458_018b74b9_2052324.png "数据监控.png")
![硬件信息监控](https://images.gitee.com/uploads/images/2019/1225/221517_79aca0f4_2052324.png "硬件信息.png")
![代码生成列表页](https://images.gitee.com/uploads/images/2019/1225/221547_530cb4c3_2052324.png "代码生成.png")
![代码生成预览页](https://images.gitee.com/uploads/images/2019/1225/221607_9685f7bc_2052324.png "代码生成-预览.png")
![操作日志列表](https://images.gitee.com/uploads/images/2019/1225/221651_86edbd04_2052324.png "操作日志.png")
![登陆日志列表](https://images.gitee.com/uploads/images/2019/1225/221712_3a05b5c6_2052324.png "登陆日志.png")

注：新加的这些功能参考[RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue)